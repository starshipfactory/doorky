package main

import (
	"flag"
	"log"
	"net/http"
	"strings"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	var readers []*GPIOReader
	var pinsToReadStr string
	var pinsToRead []string
	var pin string
	var pushgw string
	var listenTo string
	var err error

	flag.StringVar(&listenTo, "listen", ":8080",
		"(Host name and) port to listen to for web requests")
	flag.StringVar(&pushgw, "push-gw", "",
		"If set, address of a Push Gateway to push collected values to")
	flag.StringVar(&pinsToReadStr, "pins", "",
		"Comma separated list of PINs to read from and export")
	flag.Parse()

	pinsToRead = strings.Split(pinsToReadStr, ",")

	for _, pin = range pinsToRead {
		var reader *GPIOReader

		reader, err = NewGPIOReader(pin, pushgw)
		if err != nil {
			log.Fatalf("Unable to start reading PIN %s: %s", pin, err)
		}

		readers = append(readers, reader)
	}

	http.Handle("/metrics", promhttp.Handler())

	if err = http.ListenAndServe(listenTo, nil); err != nil {
		log.Fatal("Error listening to ", listenTo, ": ", err)
	}
}
