package main

import (
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
	"periph.io/x/periph/conn/gpio"
	"periph.io/x/periph/conn/gpio/gpioreg"
	"periph.io/x/periph/host"
)

// GPIOReader represents an edge-triggered reader which imports data from a
// specific GPIO pin, potentially pushing any updates to a push gateway.
type GPIOReader struct {
	pinio    gpio.PinIO
	pushgw   string
	pushSync *time.Timer
	pushMtx  *sync.Mutex
}

var pinStates *prometheus.GaugeVec
var pinLastUpdate *prometheus.GaugeVec

func init() {
	var err error

	_, err = host.Init()
	if err != nil {
		log.Print("Error initializing host: ", err)
	}

	pinStates = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Subsystem: "gpio",
			Name:      "state",
			Help:      "Current state of the given GPIO PIN",
		}, []string{"pin"})
	pinLastUpdate = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Subsystem: "gpio",
			Name:      "last_update",
			Help:      "Time of the most recent update to the GPIO state",
		}, []string{"pin"})
	prometheus.MustRegister(pinStates)
	prometheus.MustRegister(pinLastUpdate)
}

// NewGPIOReader instantiates a new GPIO reader exporting into the PIN states
// gauge vector. If an URL to a push gateway is specified, any updates will be
// pushed to that gateway.
func NewGPIOReader(pin string, pushgw string) (*GPIOReader, error) {
	var pinio = gpioreg.ByName(pin)
	var reader *GPIOReader
	var err error

	if pinio == nil {
		return nil, fmt.Errorf("No PIN of that name: %s", err)
	}
	err = pinio.In(gpio.PullUp, gpio.BothEdges)

	reader = &GPIOReader{
		pinio:  pinio,
		pushgw: pushgw,
	}
	go reader.Read()
	return reader, nil
}

// Read starts a loop of reading values from the GPIO reader using edge triggers
// and to update the corresponding PIN counter in the metric when values change.
func (r *GPIOReader) Read() {
	for {
		var lvl gpio.Level = r.pinio.Read()

		if lvl == gpio.High {
			pinStates.WithLabelValues(r.pinio.Name()).Set(1.0)
		} else {
			pinStates.WithLabelValues(r.pinio.Name()).Set(0.0)
		}
		pinLastUpdate.WithLabelValues(r.pinio.Name()).SetToCurrentTime()

		if r.pushgw != "" {
			r.pushMtx.Lock()
			// Stop any potential pending counters.
			if r.pushSync != nil {
				r.pushSync.Stop()
			}

			// Invoke update after 1 second, unless another change comes up.
			// This is so that state bounces don't cause a flood of updates to be
			// sent to the push gateway.
			r.pushSync = time.AfterFunc(time.Second, r.Push)
			r.pushMtx.Unlock()
		}

		// Wait for the next state change.
		r.pinio.WaitForEdge(-1)
	}
}

// Push sends the current value of the GPIO reader to a push gateway.
func (r *GPIOReader) Push() {
	var err error

	r.pushMtx.Lock()
	defer r.pushMtx.Unlock()

	err = push.Collectors(
		"gpio_reader", push.HostnameGroupingKey(), r.pushgw,
		pinStates, pinLastUpdate)
	if err != nil {
		log.Print("Could not push Pin state to push gateway ", r.pushgw,
			", retrying")
		r.pushSync = time.AfterFunc(time.Minute, r.Push)
		return
	}

	// Ensure that our timer is considered to be unset.
	r.pushSync = nil
}
